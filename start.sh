#!/bin/bash

export PYTHONPATH=/app/code/server/:$PYTHONPATH
set -eu

if [[ ! -d /app/data/notes ]]; then
    echo "==> Create folder on first run"
    mkdir -p /app/data/notes
fi

source /app/code/env.sh
chown -R cloudron:cloudron /app/data

echo "==> Starting App"
exec /usr/local/bin/gosu cloudron:cloudron python3 -m uvicorn server.main:app --app-dir /app/code/flatnotes --host 0.0.0.0 --port 8080 --proxy-headers --forwarded-allow-ips '*'
