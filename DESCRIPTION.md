## About

A self-hosted, database-less note taking web app that utilises a flat folder of markdown files for storage.
 
## Feature

* Mobile responsive web interface.
* Raw/WYSIWYG markdown editor modes.
* Advanced search functionality.
* Note "tagging" functionality.
* Light/dark themes.
* Multiple authentication options (none, username/password, 2FA).
* Restful API.
