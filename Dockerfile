FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ARG VERSION=4.0.2

RUN apt update && apt install -y npm

RUN mkdir -p /app/code
RUN mkdir -p /app/data/flatnotes
WORKDIR /app/code

RUN curl -L https://github.com/dullage/flatnotes/archive/refs/tags/v${VERSION}.zip -o /tmp/tmp.zip && unzip /tmp/tmp.zip -d /tmp && rm /tmp/tmp.zip
RUN mv /tmp/flatnotes-${VERSION}/* /app/code/
RUN mv /tmp/flatnotes-${VERSION}/.htmlnanorc /app/code/

RUN npm ci
RUN npm run build
RUN pip install pipenv
RUN pipenv install --deploy --ignore-pipfile --system

COPY env.sh /app/code
COPY start.sh /app/code/
EXPOSE 8080
CMD [ "/app/code/start.sh" ]
